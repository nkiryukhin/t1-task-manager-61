package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findByUserId(@NotNull String userId, @NotNull Sort sort);

    @NotNull
    Optional<ProjectDTO> findByUserIdAndId(@NotNull String userId, @NotNull String id);

}
