package ru.t1.nkiryukhin.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.model.IUserOwnedService;
import ru.t1.nkiryukhin.tm.api.service.model.IUserService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.model.AbstractUserOwnedRepository;

import java.util.Collection;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel> extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @Autowired
    protected IUserService userService;

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) return null;
        User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        model.setUser(user);
        getRepository().save(model);
        return model;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        return findOneById(userId, id) != null;
    }



    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return;
        for (M model : models) {
            removeOne(model);
        }
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        removeOne(model);
    }

}