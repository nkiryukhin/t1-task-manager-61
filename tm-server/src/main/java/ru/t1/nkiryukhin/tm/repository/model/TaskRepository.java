package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    long countByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findByUserId(@NotNull final String userId, final @NotNull Sort sort);

    @NotNull
    Optional<Task> findByUserIdAndId(@NotNull final String userId, final @NotNull String id);

}