package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nkiryukhin.tm.api.service.model.IProjectService;
import ru.t1.nkiryukhin.tm.api.service.model.ITaskService;
import ru.t1.nkiryukhin.tm.api.service.model.IUserService;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.configuration.ServerConfiguration;
import ru.t1.nkiryukhin.tm.enumerated.CustomSorting;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.migration.AbstractSchemeTest;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.USUAL_PROJECT1;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private static final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private static final ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private static final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private static final Liquibase liquibase = liquibase("changelog/changelog-master.xml");

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void loadTestData() {
        userService.add(USUAL_USER);
        userService.add(ADMIN_USER);
        projectService.add(USUAL_PROJECT1);
        taskService.add(USUAL_TASK1);
        taskService.add(USUAL_TASK2);
    }

    @After
    public void removeTestData() throws AbstractException {
        for (Task task : TASK_LIST) {
            taskService.removeById(task.getUser().getId(), task.getId());
        }
        projectService.removeById(USUAL_PROJECT1.getUser().getId(), USUAL_PROJECT1.getId());
        userService.removeById(ADMIN_USER.getId());
        userService.removeById(USUAL_USER.getId());
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertNull(taskService.add(ADMIN_USER.getId(), NULL_TASK));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.add(null, ADMIN_TASK1);
        });
        Assert.assertNotNull(taskService.add(ADMIN_USER.getId(), ADMIN_TASK1));
        @Nullable final Task task = taskService.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.changeTaskStatusById(null, USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.changeTaskStatusById("", USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.changeTaskStatusById(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.changeTaskStatusById(USUAL_USER.getId(), "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            taskService.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            taskService.changeTaskStatusById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, status);
        });
        taskService.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), status);
        Task updatedTask = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(status, updatedTask.getStatus());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.clear("");
        });
        taskService.clear(USUAL_USER.getId());
        Assert.assertEquals(0, taskService.getSize(USUAL_USER.getId()));
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(taskService.existsById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(taskService.existsById(USUAL_USER.getId(), USUAL_TASK1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.findAll("");
        });
        Assert.assertEquals(USUAL_TASK_LIST, taskService.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findAllSortByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable CustomSorting sortInner = null;
            taskService.findAll("", sortInner);
        });
        CustomSorting sort = CustomSorting.BY_NAME;
        Assert.assertEquals(USUAL_TASK_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), taskService.findAll(USUAL_USER.getId(), sort));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.existsById(null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.existsById("", USUAL_TASK1.getId());
        });
        Assert.assertNull(taskService.findOneById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void getSize() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.getSize("");
        });
        Assert.assertEquals(0, taskService.getSize(ADMIN_USER.getId()));
        taskService.add(ADMIN_TASK1);
        Assert.assertEquals(1, taskService.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.removeById(USUAL_USER.getId(), "");
        });
        @Nullable final Task createdTask = taskService.add(ADMIN_TASK1);
        Assert.assertTrue(taskService.existsById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
        taskService.removeById(ADMIN_USER.getId(), createdTask.getId());
        Assert.assertNull(taskService.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void clear() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            String userId = null;
            taskService.clear(userId);
        });
        Assert.assertTrue(taskService.getSize(USUAL_USER.getId()) > 0);
        taskService.clear(USUAL_USER.getId());
        Assert.assertTrue(taskService.getSize(USUAL_USER.getId()) == 0);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.create(null, ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.create("", ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            taskService.create(ADMIN_USER.getId(), null, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            taskService.create(ADMIN_USER.getId(), "", null);
        });
        @NotNull final Task task = taskService.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), "name");
        Task createdTask = taskService.findOneById(ADMIN_USER.getId(), task.getId());
        Assert.assertEquals(task, createdTask);
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_USER, task.getUser());
        TASK_LIST.add(task);
    }

    @Test
    public void findAllByProjectId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.findAllByProjectId(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.findAllByProjectId("", USUAL_PROJECT1.getId());
        });
        @NotNull final Collection<Task> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, taskService.findAllByProjectId(USUAL_USER.getId(), null));
        Assert.assertEquals(emptyCollection, taskService.findAllByProjectId(USUAL_USER.getId(), ""));
        Assert.assertEquals(USUAL_TASK_LIST, taskService.findAllByProjectId(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.updateById(null, USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            taskService.updateById("", USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.updateById(USUAL_USER.getId(), null, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            taskService.updateById(USUAL_USER.getId(), "", USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            taskService.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), null, USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            taskService.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), "", USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            taskService.updateById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        @NotNull final String name = USUAL_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USUAL_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        taskService.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), name, description);
        Task updatedTask = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(description, updatedTask.getDescription());
    }

}
