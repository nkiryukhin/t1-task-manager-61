package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResponse {
}