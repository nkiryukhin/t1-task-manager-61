package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @Column
    @NotNull
    private Date created = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(getId(), session.getId())
                && Objects.equals(created, session.created)
                && Objects.equals(role, session.role);
    }

}