package ru.t1.nkiryukhin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.model.IListener;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show argument list";
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        for (final IListener listener : listeners) {
            @Nullable final String argument = listener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
