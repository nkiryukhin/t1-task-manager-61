package ru.t1.nkiryukhin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.model.IListener;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list";
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        for (final IListener listener : listeners) {
            @NotNull final String name = listener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
